from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from ims_staff.services import staffid_generator
from reusable_models import get_model_from_string

StaffDesignation = get_model_from_string("STAFF_DESIGNATION")
Designation = get_model_from_string("DESIGNATION")
Staff = get_model_from_string("STAFF")


@receiver(post_save, sender=User)
def add_to_staff_group(instance, **kwargs):
    if instance.is_superuser:
        staff = Staff.objects.get_or_create(
            user=instance, defaults={"staffid": staffid_generator()}
        )
        admin_designation = Designation.objects.get(name="Admin")
        StaffDesignation.objects.get_or_create(
            staff=staff[0],
            designation=admin_designation,
            defaults={
                "is_active": True,
                "is_permanent": True,
                "is_default": True,
            },
        )
