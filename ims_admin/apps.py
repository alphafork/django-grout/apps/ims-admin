from django.apps import AppConfig


class IMSAdminConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "ims_admin"
